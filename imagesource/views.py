import os
from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.http import HttpResponseBadRequest
from imagesource.models import ImageSource
from imagesource.appcfg import Const
from util.fileutil import listdir_by_name

def index(request):
    sources = ImageSource.objects.order_by('-name')
    context = {'sources': sources}
    return render(request, 'imagesource/index.html', context)

def select_view(request):
    sources = ImageSource.objects.order_by('-name')
    context = {'sources': sources}
    return render(request, 'imagesource/imagesource_select.html', context)

def select(request):
    sources = ImageSource.objects.order_by('-name')
    context = {'sources': sources}
    return render(request, 'imagesource/imagesource_select.html', context)

def imagesource(request, imagesource_id):
    imgsrc = get_object_or_404(ImageSource, id=imagesource_id)
    context = {'imagesource': imgsrc}
    return render(request, 'imagesource/imagesource.html', context)

def imageview(request, imagesource_id, index):
    '''Reads .jpg files in directory, orders them my name, then renders the
    image in a page with Prev and Next links.  Prev/Next links implements 
    a circular queue.'''
    index = int(index)
    imgsrc = get_object_or_404(ImageSource, id=imagesource_id)
    jpglist = listdir_by_name(os.path.join(settings.MEDIA_ROOT, imgsrc.path),
                              ('.jpg',))
    size = len(jpglist)
    if index >= size:
        index = 0
    prev_index = size - 1 if index <= 0 else index - 1
    next_index = index + 1 if index < size-1 else 0
    image_url = settings.MEDIA_URL + imgsrc.path
    if not str(image_url).endswith('/'):
        image_url += '/'
    image_url += jpglist[index]
    context = {'imagesource': imgsrc, "image_url": image_url,
               "prev_index": prev_index, "next_index": next_index, 
               "size": size, "index": index}
    return render(request, 'imagesource/imageview.html', context)
    #return HttpResponseBadRequest('index %d out of bounds: (%d, %d)'%(index, 0, size))