# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

sources = ( ("audio_PreampMixer dutyCycleStates", "audio_PreampMixer/dutyCycleStates"),
           
            ("doorLocks_front-3fps", "doorLocks_front/doorLocks-front-3fps"),
            ("doorLocks_front dutyCycleStates", "doorLocks_front/dutyCycleStates"),
            
            ("doorLocks_Obliq-3fps", "doorLocks_Obliq/doorLocks-Oblq-3fps"),
            ("doorLocks_Obliq/dutyCycleStates", "doorLocks_Obliq/dutyCycleStates"),
            
            ("lightSources/dutyCycleStates", "lightSources/dutyCycleStates"),
            ("lightSources-3fps", "lightSources/lightSources-3fps"),
            
            ("phoneDslPlug/phoneDslPlug-3fps", "phoneDslPlug/phoneDslPlug-3fps"),
            ("phoneDslPlug/phoneDslPlug-states", "phoneDslPlug/phoneDslPlug-states"),
            
            ("powerPlugs/powerPlugs-3fps", "powerPlugs/powerPlugs-3fps"),
            ("powerPlugs/powerPlugs-states", "powerPlugs/powerPlugs-states"),
            
            ("streetLights/dutyCycleStates", "streetLights/dutyCycleStates"),
            
            ("wallSwitches_front/dutyCycleStates", "wallSwitches_front/dutyCycleStates"),
            ("wallSwitches_front/wallSwitches-front-3fps", "wallSwitches_front/wallSwitches-front-3fps"),
            
            ("wallSwitches_Obliq/dutyCycleStates", "wallSwitches_Obliq/dutyCycleStates"),
            ("wallSwitches_Obliq/wallSwitches-Oblq-3fps", "wallSwitches_Obliq/wallSwitches-Oblq-3fps"),
            
            ("workstation dutyCycleStates", "workstation/dutyCycleStates"),
            ("workstation-3fps", "workstation/workstation-3fps"),
            )

class SeleniumAdminDbCreate(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://127.0.0.1:8000/admin/"
        self.media_url = "http://127.0.0.1:8000/media/"
        self.verificationErrors = []
        self.accept_next_alert = True
        self.user = "forrest"
        self.passwd = "P@ssw0rd"
    
    def go_home_app(self):
        driver = self.driver
        driver.find_element_by_link_text("Home").click()
        driver.find_element_by_link_text("Imagesource").click()
    
    def user_login(self):
        driver = self.driver
        driver.get(self.base_url)
        driver.find_element_by_id("id_username").clear()
        driver.find_element_by_id("id_username").send_keys(self.user)
        driver.find_element_by_id("id_password").clear()
        driver.find_element_by_id("id_password").send_keys(self.passwd)
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()
        driver.find_element_by_link_text("Imagesource").click()
        
    def add_Imagesource(self, name, srctype, path):
        self.go_home_app()
        driver = self.driver
        driver.find_element_by_link_text("Image sources").click()
        driver.find_element_by_link_text("Add image source").click()
        driver.find_element_by_id("id_name").clear()
        driver.find_element_by_id("id_name").send_keys(name)
        Select(driver.find_element_by_id("id_srctype")).select_by_visible_text(srctype)
        driver.find_element_by_id("id_path").clear()
        driver.find_element_by_id("id_path").send_keys(path)
        driver.find_element_by_name("_save").click()
        self.assertIn('was added successfully', 
                      driver.find_element_by_class_name('success').text,
                      'Imagesource name="%s"'%name)
        pass
    
    def add_Imagesources(self):
        for name, path in sources:
            self.add_Imagesource(name, "Directory", path)
            
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
        
    def test_admin_init(self):
        self.user_login()
        self.add_Imagesources()
        
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
