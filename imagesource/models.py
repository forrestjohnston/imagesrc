from django.db import models
from django.conf import settings
from appcfg import Const

class ImageSource(models.Model):
    name     = models.CharField(max_length=200)
    srctype  = models.CharField(max_length=24,
                                    choices=Const.SOURCE_TYPE_CHOICES,
                                    default=Const.SOURCE_TYPE_URL)
    path = models.CharField(max_length=200)
    
    def to_dict(self):
        mydict = dict(id = self.id, 
                      name = self.name, 
                      srctype = self.srctype)
        if self.srctype == Const.SOURCE_TYPE_DIRECTORY:
            mydict['path'] = settings.MEDIA_URL + self.path
        else:
            mydict['path'] = self.path
        return mydict
    
    def __unicode__(self):
        return self.name
    
