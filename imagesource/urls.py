from django.conf.urls import patterns, url
from imagesource import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^select/view/$', views.select_view, name='select_view'),
    url(r'^select/$', views.select, name='select'),
    # ex: /imagesource/5/
    url(r'^(?P<imagesource_id>\d+)/$', views.imagesource, name='imagesource'),
    url(r'^(?P<imagesource_id>\d+)/(?P<index>\d+)/$', views.imageview, name='imageview'),
)
