#!/usr/bin/python
# Filename: appConfig.py

def singleton(cls):
    obj = cls()
    cls.__new__ = lambda cls: obj  # Always return the same object
    return cls

@singleton
class Const(object):
    SOURCE_TYPE_URL = "URL"  
    SOURCE_TYPE_DIRECTORY = "Directory"  
    SOURCE_TYPE_CHOICES = (
        (SOURCE_TYPE_URL, SOURCE_TYPE_URL),
        (SOURCE_TYPE_DIRECTORY, SOURCE_TYPE_DIRECTORY),
    )

    