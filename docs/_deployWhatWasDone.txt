$ adduser forrest sudo
$ su root
$ sudo passwd root
$ exit

$ sudo apt-get install xrdp
$ echo "gnome-session --session=ubuntu-2d" > ~/.xsession
$ sudo apt-get install gnome-session-fallback
$ sudo shutdown -r now

$ sudo apt-get install git-core
$ git config --global user.name "forrest"
$ git config --global user.email forrest@forrestJohnston.com
$ cat ~/.gitconfig

# Save what we have done to a log
$ history > ~/Desktop/whatWasDone.txt

sudo apt-get install tree

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python-pip python-dev build-essential
sudo pip install --upgrade pip
sudo pip install virtualenvwrapper

echo "export WORKON_HOME=$HOME/.virtualenvs" >> ~/.bash_profile
echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bash_profile
echo "source ~/.bashrc" >> ~/.bash_profile
source ~/.bash_profile

mkvirtualenv imagesrcenv --no-site-packages
mkdir -p ~/django_projects/imagesrc
cd ~/django_projects
git clone https://github.com/Forrestjohnston/imagesrc.git

cd ~/django_projects/imagesrc
workon imagesrcenv
pip install -r requirements.txt

rm -f mytmp; cat <<EOT >> mytmp
<VirtualHost *:80>
    ServerAdmin webmaster@northbaysoftware.com
    ServerName northbaysoftware.com
    ServerAlias www.northbaysoftware.com
    WSGIScriptAlias / var/www/northbaysoftware.com/index.wsgi
    Alias /static/ /var/www/northbaysoftware.com/static/
    Location "/static/">
    	Options -Indexes
    Location >
</VirtualHost >
EOT
sudo cat mytmp | sudo tee /etc/apache2/sites-available/northbaysoftware.com
rm -f mytmp


rm -f mytmp; cat <<EOT >> mytmp
import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('~/.virtualenvs/imagesrcenv/local/lib/python2.7/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/home/forrest/django_projects/imagesrc')
sys.path.append('/home/forrest/django_projects/imagesrc/imagesrc')

os.environ['DJANGO_SETTINGS_MODULE'] = 'imagesrc.settings'

# Activate your virtual env
activate_env=os.path.expanduser("~/.virtualenvs/imagesrcenv/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
EOT
cat mytmp | sudo tee /var/www/northbaysoftware.com/index.wsgi

sudo mkdir -p /var/www/northbaysoftware.com/static
sudo chown -R forrest /var/www/northbaysoftware.com
python manage.py collectstatic



